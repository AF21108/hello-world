import java.util.Scanner;
public class ex4 {
    static void order(String food){
        System.out.println("You have ordered "+food+". Thank you!");
    }
    public static void main(String[] args) {
        String[] menu = {"Tempura","Ramen","Udon"};
        System.out.println("What would you like to order:");
        int number;
        for(int i=1;i<=menu.length;i++){
            System.out.println(i+". "+menu[i-1]);
        }
        Scanner userIn = new Scanner(System.in);
        while(true) {
            number = userIn.nextInt();
            if(number>0&&number<=menu.length)
                break;
            System.out.println("Plese input 1~"+(menu.length));
        }
        userIn.close();
        order(menu[number-1]);
    }
}